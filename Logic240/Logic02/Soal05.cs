﻿using Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic02
{
    class Soal05 : LogicProps
    {
        public Soal05(string kalimat)
        {
            Array1D = new string[kalimat.Length];
            FillArray(kalimat);
            Console.WriteLine(kalimat);
            Console.WriteLine("\n");
            PrintFrontBack();
            Console.ReadKey();
        }

        private void PrintFrontBack()
        {
            for (int i = 0; i < Array1D.Length; i++)
            {
                if (i == 0 || 
                    i == Array1D.Length - 1 || 
                    Array1D[i - 1] == " " || 
                    Array1D[i + 1] == " " || 
                    Array1D[i] == " ")
                    Console.Write(Array1D[i]);
                else
                    Console.Write("*");
            }
        }

        private void FillArray(string kal)
        {
            char[] cArr = kal.ToCharArray();
            int idx = 0;
            foreach (var ch in cArr)
            {
                Array1D[idx++] = ch.ToString();
            }
        }
    }
}
